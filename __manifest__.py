# Copyright 2019 Patrick Wilson <patrickraymondwilson@gmail.com>
# License LGPLv3.0 or later (https://www.gnu.org/licenses/lgpl-3.0.en.html).

{
    'name': "Project Templates Timedelta",
    'summary': """Project Templates Timedelta""",
    'author': "César López Ramírez - Coopdevs",
    'website': "http://coopdevs.org",
    'category': 'Project Management',
    'version': '12.0.1.0.1',
    'license': 'AGPL-3',
    'depends': ['project_template'],
    'data': [
    ],
    'application': False,
}
