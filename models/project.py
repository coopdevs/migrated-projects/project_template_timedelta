# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).
from odoo import models, fields, api
from datetime import datetime, timedelta


class Project(models.Model):
    _inherit = 'project.project'

    def create_project_from_template(self):
        if " (TEMPLATE)" in self.name:
            new_name = self.name.replace(" (TEMPLATE)", " (COPY)")
        else:
            new_name = self.name + " (COPY)"
        new_project = self.copy(default={'name': new_name,
                                         'active': True,
                                         'total_planned_hours': 0.0,
                                         'alias_name': False})
        if new_project.subtask_project_id != new_project:
            new_project.subtask_project_id = new_project.id

        # SINCE THE END DATE DOESN'T COPY OVER ON TASKS
        # (Even when changed to copy=true), POPULATE END DATES ON THE TASK
        for new_task_record in new_project.task_ids:
            for old_task_record in self.task_ids:
                if new_task_record.name == old_task_record.name:
                    date_start = datetime.utcnow()
                    new_task_record.date_start = date_start
                    delta = (
                        old_task_record.date_end.date() -
                        old_task_record.date_start.date()
                    )
                    delta_days = delta.days
                    delta_hours = round(delta.seconds/3600)
                    new_task_record.date_end = (
                        date_start+
                        timedelta(days=delta_days)+
                        timedelta(hours=delta_hours)
                    )

        # OPEN THE NEWLY CREATED PROJECT FORM
        return {
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'project.project',
            'target': 'current',
            'res_id': new_project.id,
            'type': 'ir.actions.act_window'
        }
